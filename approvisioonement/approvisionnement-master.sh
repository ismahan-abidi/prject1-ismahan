#!/bin/bash
########################## create workspace #################################
  mkdir -p /var/jenkins_home/aws/puppet   
########################## create key pair ##################################
aws ec2 describe-key-pairs --key-name MyKeyPair
if [ $? != 0  ]
then
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > /var/jenkins_home/aws/puppet/MyKeyPair.pem
chmod 400 /var/jenkins_home/aws/puppet/MyKeyPair.pem
mail -s "key pair" ismahen.abidi1992@gmail.com < /var/jenkins_home/aws/puppet/MyKeyPair.pem
else 
    echo "************************ key pair already exist *****************************"
fi

########################## create security group ##################################
idSG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=SG_MASTER  --query "SecurityGroups[*].[GroupId]" --output text)
if [ -z "$idSG" ]
then 
echo "************************** security group does not exit , we will create it **********************************"
aws ec2 create-security-group --group-name SG_MASTER --description "My security group for the PUPPET master"
idSG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=SG_MASTER  --query "SecurityGroups[*].[GroupId]" --output text)
aws ec2 authorize-security-group-ingress --group-id $idSG --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $idSG --protocol tcp --port 8140 --cidr 0.0.0.0/0
aws ec2 create-tags --resources $idSG --tags 'Key=Name,Value=SG_MASTER'
else 
echo "***************************** security group already exist *****************************************************"
fi

########################### create ec2 ##########################################
InstanceId=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag-value,Values=PUPPET_MASTER" | grep -i InstanceId | cut -d \" -f4)
# aws ec2 terminate-instances --instance-ids $InstanceId
#echo " ******************************** ec2 instance deleted with success ****************************"
if [ -z "$InstanceId" ]
then 
echo "**************** ec2 does not exist, we will create it ********************"
aws ec2 run-instances --image-id ami-03b755af568109dc3 --count 1 --instance-type t2.medium --key-name MyKeyPair --security-group-ids $idSG \
 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=PUPPET_MASTER}]'  > /var/jenkins_home/aws/puppet/master_instance
echo "********************* waiting 60 seconds for instance to be started ***********************"
 sleep 60
InstanceId=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag-value,Values=PUPPET_MASTER" | grep -i InstanceId | cut -d \" -f4)
 echo "------------------------ id de l'instance ec2 est $InstanceId ----------------------------------------------"
 #aws ec2 create-tags --resources $InstanceId --tags Key=Name,Value=PUPPET_MASTER
 echo "installation terminate"
 else 
 echo "********************* ec2 instance $InstanceId exist **************************************"
 fi
 public_ipv4_master=$(aws --region eu-west-3 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=instance-id,Values=$InstanceId" \
--query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text)
echo "**the public ip of the instance ec2 master is $public_ipv4_master "| mail -s "public ip"  ismahen.abidi1992@gmail.com
#copie de site.pp et init.pp du container jenkins to node master
scp -o "StrictHostKeyChecking no" -i /var/jenkins_home/aws/puppet/MyKeyPair.pem puppet/site.pp ubuntu@$public_ipv4_master:/home/ubuntu
scp -o "StrictHostKeyChecking no" -i /var/jenkins_home/aws/puppet/MyKeyPair.pem puppet/installation/manifests/init.pp ubuntu@$public_ipv4_master:/home/ubuntu  
ssh  -o StrictHostKeyChecking=no -tt -i /var/jenkins_home/aws/puppet/MyKeyPair.pem ubuntu@$public_ipv4_master   'bash -s' < approvisioonement/installation_master.sh
echo "--------------rebonjour jenkins------------------"
