#!/bin/bash 
user=$1
sudo su 
# adresse ip privée pour le mettre dans /etc/hosts de puppet
private_ip_agent=$(ip a | grep eth0 | cut -d " " --fields=6 | sed '2q;d' | awk -F'/' '{print $1}')
echo " .................. the private ip of master node is $private_ip_agent ........................"
apt update -y 
wget https://apt.puppetlabs.com/puppet7-release-focal.deb
dpkg -i puppet7-release-focal.deb
apt-get update -y
echo "installation de puppet agent va commencer .........................................."
nohup apt-get install puppet-agent -y &
echo " $private_ip_agent puppet " >> /etc/hosts
# changer le nom de la certif
echo '''
[main]
certname = cert_agent_$user
''' >> /etc/puppetlabs/puppet/puppet.conf
# définir le binaire de puppet dans bashrc
echo 'puppet=/opt/puppetlabs/bin/' >> ~/.bashrc
echo 'PATH=$PATH:$puppet' >> ~/.bashrc
source ~/.bashrc
#activer puppet agent 
systemctl start puppet 
systemctl enable puppet --now 
# supprimer les enciennes signatures
rm -rf /etc/puppetlabs/puppet/ssl/*
puppet agent -t 
sleep 30
#Le hostname de la machine devra être nommée nuvola.
echo "nuvola" > /etc/hostname
echo "bye bye ec2 dev puppet agent"
nohup exit;  &
exit;