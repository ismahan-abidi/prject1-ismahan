#!/bin/bash
user=$1
email=$2
echo " #######################create workspace ####################################  "
mkdir -p /var/jenkins_home/aws/puppet
echo "########################## create key pair ##################################  "
aws ec2 describe-key-pairs --key-name MyKeyPair_agent_$user
if [ $? != 0 ]
then
    echo " private key does not exist , we will create it "
    aws ec2 create-key-pair --key-name MyKeyPair_agent_$user --query 'KeyMaterial' --output text > /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem
    chmod 400 /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem
    #The -A flag defines attachment of the file
    echo "you find in the attachement the private key of the puppet dev" | mail -s "Puppet dev : private key" $email -A /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem
    echo "private key is already created and sent by email, check your email box please"
else
    echo " private key already exist  "
fi
echo "########################## create security group  ##################################  "
idSG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=SG_AGENT_$user  --query "SecurityGroups[*].[GroupId]" --output text)
if [ -z "$idSG" ]
then
    echo " ######################### security group does not exist , we will create it ###########"
    aws ec2 create-security-group --group-name SG_AGENT_$user --description "My security group AGENT"
    idSG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=SG_AGENT_$user  --query "SecurityGroups[*].[GroupId]" --output text)
    aws ec2 authorize-security-group-ingress --group-id $idSG --protocol tcp --port 22 --cidr 0.0.0.0/0
    aws ec2 authorize-security-group-ingress --group-id $idSG --protocol tcp --port 8140 --cidr 0.0.0.0/0
    aws ec2 create-tags --resources  $idSG --tags Key=Name,Value=SG_AGENT_$user
    echo " ################## success creating SG ###########################"
else
    echo " ##############  security group already created ###################"
fi
echo " ############################ create instance ec2 dev ######################"
id_ec2_agent=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag-value,Values=AGENT_DEV"  | grep -i instanceId | cut -d \" -f4)
if [ -z "$id_ec2_agent" ]
then
    aws ec2 run-instances --image-id ami-03b755af568109dc3 --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=AGENT_DEV}]" \
    --count 1 --instance-type t2.micro --key-name MyKeyPair_agent_$user --security-group-ids $idSG
    
    echo "**********************************\n waiting 30s for instance to be ready \n**********************************"
    sleep 30
    echo "######################## installation terminated #############################"
    id_ec2_agent=$(aws ec2 describe-instances --filters  "Name=instance-state-name,Values=running" "Name=tag-value,Values=AGENT_DEV"| grep -i instanceId | cut -d \" -f4)
    echo "##################### the new ec2 id is $id_ec2_agent  ###########################"
    public_ipv4_agent=$(aws --region eu-west-3 ec2 describe-instances --filters \
    "Name=instance-state-name,Values=running" "Name=instance-id,Values=$id_ec2_agent" \
    --query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text)
    echo "############################# the public ip of ec2 agent is $public_ipv4_agent ################"

ssh  -o StrictHostKeyChecking=no -tt -i /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem ubuntu@$public_ipv4_agent 'bash -s' < approvisioonement/installation-agent.sh
 # configuration desc certif au niveau du master                       
    echo """  
    #!/bin/bash
     sudo su
     puppetserver ca list --all
     puppetserver ca sign --certname cert_agent_$user
     echo '
     
     node cert_agent_$user {
         include installation 
         } 
         
         ' >> /etc/puppetlabs/code/environments/production/manifests/site.pp
         exit;
         exit;
    """ > sign_certif_$user
    # récupérer l'ipv4 du master pour se connecter dedans pour la demnde de certif 
    public_ipv4_master=$(aws --region eu-west-3 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=instance-id,Values=$InstanceId" \
--query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text)
ssh  -o StrictHostKeyChecking=no -tt -i /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem ubuntu@$public_ipv4_master   'bash -s' < sign_certif_$user
 #installation node js et docker dans la machine dev
    echo """
    #!/bin/bash
    sudo su
    puppet agent -t
    docker pull node:14-alpine
    docker pull mongo:4.4
    reboot
    """ > installation_dependencies_$user
    ssh -o "StrictHostKeyChecking no" -tt -i /var/jenkins_home/aws/puppet/MyKeyPair_agent_$user.pem  ubuntu@$public_ipV4_agent_node "bash -s" < installation_dependencies_$user
else
    echo "############################ instance already exist ####################"
fi
echo " bienvenue dans le conteneur jenkins de nouveau"


